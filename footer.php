
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      
      <p class="m-0 text-center text-white">Копирование данного сайта, написаного на экзамене, преследуется по закону совести</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery.min.js"></script>
  <script src="bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

</body>

</html>
