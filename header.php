<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web-reg main page</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="bootstrap.min.css">

  <!-- Custom styles for this template -->
  <link href="shop-homepage.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <img src="https://www.ucheba.ru/pix/logo_cache/5868.upto100x100.png" alt="Лого политеха" style="width: 25px; height: 25px; margin-right: 10px">
      <a class="navbar-brand" href="#">Продажа доменов</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/index.php">На главную
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Домены</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/hosting.php">Хостинг</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Сервисы</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>